FROM alpine:3.16

MAINTAINER cybernemo

ENV JAVA_HOME /usr/lib/jvm/java-11-openjdk
ENV PATH $JAVA_HOME/bin:$PATH

RUN \
        apk update && \
        apk upgrade

RUN \
        apk add openjdk11

